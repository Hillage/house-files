#include <stdio.h>
#include <stdlib.h>

int main() {
    FILE *h;
    h = fopen("homelistings.csv", "r");
    
    if (!h) {
        printf("Couldn't open file\n");    
    }
    
    int min = 0;
    int max = 0;
    int total = 0;
    
    //zip, id, address, price, bed, bath, area
    char address[30];
    int zip, id, price, bed, bath, area;
    int count = 0;
    
    // Small <1000, Med 1000-2000, Large >2000
    // 123 Main St. : 1734
    while (fscanf(h, " %d,%d,%[^,],%d,%d,%d,%d", &zip, &id, address, &price, &bed, &bath, &area) != EOF) {
        if (area < 1000) {
            FILE *s;
            s = fopen("small.txt", "a");
            
            if (!s) {
                printf("Couldn't open small.txt\n");    
            }
            
            fprintf(s, "%d\n", area);
            
            fclose(s);
        } else if (area > 2000) {
            FILE *l;
            l = fopen("large.txt", "a");
            
            if (!h) {
                printf("Couldn't open large.txt\n");    
            }
            
            fprintf(l, "%d\n", area);
            
            fclose(l);
        } else {
            FILE *m;
            m = fopen("med.txt", "a");
            
            if (!h) {
                printf("Couldn't open med.txt\n");    
            }
            
            fprintf(m, "%d\n", area);
            
            fclose(m);
        }
    }
    
    
    
    fclose(h);
    
}
