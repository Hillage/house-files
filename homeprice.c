#include <stdio.h>
#include <stdlib.h>

int main() {
    FILE *h;
    h = fopen("homelistings.csv", "r");
    
    if (!h) {
        printf("Couldn't open file\n");    
    }
    
    int min = 0;
    int max = 0;
    long long  total = 0;
    
    //zip, id, address, price, bed, bath, area
    char address[40];
    int zip, id, bed, bath, area;
    int price;
    int count = 0;
    while (fscanf(h, " %d,%d,%[^,],%d,%d,%d,%d", &zip, &id, address, &price, &bed, &bath, &area) != EOF) {
        if (min == 0 || price < min) {
            min = price;
        }
        
        if (price > max) {
            max = price;
        }
        
        total += price;
        count++;
    }
    
    long long average = total/count;
    
    printf("%d %d %lld\n", min, max, average);
    
    fclose(h);
    
}
