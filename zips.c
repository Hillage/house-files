#include <stdio.h>
#include <stdlib.h>

int main() {
    FILE *h;
    h = fopen("homelistings.csv", "r");
    
    if (!h) {
        printf("Couldn't open file\n");    
    }
    
    int min = 0;
    int max = 0;
    int total = 0;
    
    //zip, id, address, price, bed, bath, area
    char address[30];
    int zip, id, price, bed, bath, area;
    int count = 0;
    
    // Small <1000, Med 1000-2000, Large >2000
    // 123 Main St. : 1734
    while (fscanf(h, " %d,%d,%[^,],%d,%d,%d,%d", &zip, &id, address, &price, &bed, &bath, &area) != EOF) {
    
            char filename[10];
            sprintf(filename, "%d.txt", zip);
            
            FILE *z;
            z = fopen(filename, "a");
            
            if (!z) {
                printf("Couldn't open %s\n", filename);    
            }
            
            fprintf(z, "%s\n", address);
            
            fclose(z);
            
     }
    
    fclose(h);
    
}
